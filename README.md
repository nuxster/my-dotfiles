# The repository contains my dotfiles.
### !!!Be careful!!!

**Tiling WMs**
----
- [Dotfiles for Wayland with Sway ➤](https://gitlab.com/nuxster/my-dotfiles/-/tree/wayland-sway)
- [Dotfiles for Xorg with AwesomeWM ➤](https://gitlab.com/nuxster/my-dotfiles/-/tree/xorg-awesomewm)
<!-- blank line -->

**DE**
----
- [Dotfiles for Wayland with Gnome ➤](https://gitlab.com/nuxster/my-dotfiles/-/tree/wayland-gnome)
<!-- blank line -->

**Tools**
----
- [Dotfiles for neovim-lua ➤](https://gitlab.com/nuxster/my-dotfiles/-/tree/neovim-lua)
- [Dotfiles for tmux ➤](https://gitlab.com/nuxster/my-dotfiles/-/tree/tmux)
<!-- blank line -->

**etc**
----
- [Useful scripts ➤](https://gitlab.com/nuxster/my-dotfiles/-/tree/useful-scripts)
<!-- blank line -->
